import BaseQuestionSubject from './TQuestionSubject';
import BaseOption from './BaseOption';

// 排序选项
export class RankSelOption extends BaseOption {
    // 排序的位置
    public OrderIndex: number = 0;
    public HasChecked: boolean = false;
}
export default class RankSubject extends BaseQuestionSubject<RankSelOption> {
    public static option: RankSelOption;


}
